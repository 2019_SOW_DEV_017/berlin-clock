var BerlinClock = require("../src/BerlinClock");

var berlinClock = new BerlinClock();

describe("Berlin Clock", function () {

    describe("Bottom Minutes Row", function () {

        it("should all lamps are OFF when no minute left from top minutes row", function () {

            expect(berlinClock.bottomMinutes(35)).toEqual("OOOO");
        });

        it("should first yellow lamp is ON when one minute left from top minutes row", function () {

            expect(berlinClock.bottomMinutes(56)).toEqual("YOOO");
        });

        it("should first two yellow lamps are ON when two minutes left from top minutes row", function () {

            expect(berlinClock.bottomMinutes(7)).toEqual("YYOO");
        });

        it("should first three yellow lamps are ON when three minutes left from top minutes row", function () {

            expect(berlinClock.bottomMinutes(3)).toEqual("YYYO");
        });

        it("should all yellow lamps are ON when four minutes left from top minutes row", function () {

            expect(berlinClock.bottomMinutes(59)).toEqual("YYYY");
        });
    });

    describe("Top Minutes Row", function () {

        it("should all lamps are OFF when minutes less than five", function () {

            expect(berlinClock.topMinutes(3)).toEqual("OOOOOOOOOOO");
        });

        it("should all lamps are ON with every third lamp is red and others are in yellow when minutes greater than 54", function () {

            expect(berlinClock.topMinutes(59)).toEqual("YYRYYRYYRYY");
        });

        it("should first red lamp is ON when more than 14 minutes", function () {

            expect(berlinClock.topMinutes(18)).toEqual("YYROOOOOOOO");
        });

        it("should two red lamps are ON when more than 29 minutes", function () {

            expect(berlinClock.topMinutes(35)).toEqual("YYRYYRYOOOO");
        });
    });

    describe("Bottom Hours Row", function () {

        it("should all lamps are OFF when no hour left from top hours row", function () {

            expect(berlinClock.bottomHours(15)).toEqual("OOOO");
        });

        it("should first red lamp is ON when one hour left from top hours row", function () {

            expect(berlinClock.bottomHours(1)).toEqual("ROOO");
        });

        it("should first two red lamps are ON when two hours left from top hours row", function () {

            expect(berlinClock.bottomHours(7)).toEqual("RROO");
        });

        it("should three red lamps are ON when three hours left from top hours row", function () {

            expect(berlinClock.bottomHours(13)).toEqual("RRRO");
        });

        it("should all red lamps are ON when four hours left from top hours row", function () {

            expect(berlinClock.bottomHours(19)).toEqual("RRRR");
        });
    });

    describe("Top Hours Row", function () {

        it("should all lamps are OFF when hour is less than five", function () {

            expect(berlinClock.topHours(4)).toEqual("OOOO");
        });

        it("should all red lamp are ON when hour more than 19", function () {

            expect(berlinClock.topHours(23)).toEqual("RRRR");
        });

        it("should first red lamp is ON when hour is 5 to 9", function () {

            expect(berlinClock.topHours(9)).toEqual("ROOO");
        });

        it("should first two red lamps are ON when hour is 10 to 14", function () {

            expect(berlinClock.topHours(10)).toEqual("RROO");
        });

        it("should first three red lamps are ON when hour is 15 to 19", function () {

            expect(berlinClock.topHours(17)).toEqual("RRRO");
        });
    });

    describe("Seconds Lamp", function () {

        it("should yellow lamp is OFF when seconds are odd", function () {

            expect(berlinClock.secondsLamp(59)).toEqual("O");
        });

        it("should yellow lamp is ON when seconds are even", function () {

            expect(berlinClock.secondsLamp(2)).toEqual("Y");
        });
    });

    describe("Entire Berlin Clock", function () {

        it("should get error message when time format is invalid", function () {

            expect(berlinClock.clock("24:00:01")).toEqual("Invalid time format");
        });

        it("should all top hours row, three lamps in bottom hours row and all minutes row lamps are ON when 23:59:59", function () {

            expect(berlinClock.clock("23:59:59")).toEqual("ORRRRRRROYYRYYRYYRYYYYYY");
        });

        it("should all lamp are OFF when 00:00:01", function () {

            expect(berlinClock.clock("00:00:01")).toEqual("OOOOOOOOOOOOOOOOOOOOOOOO");
        });

        it("should three lamps in top hours row, all bottom hours, all minutes and seconds lamps are ON when 19:59:00", function () {

            expect(berlinClock.clock("19:59:00")).toEqual("YRRRORRRRYYRYYRYYRYYYYYY");
        });
    });
});
