var LAMP_TOTAL = {
    BOTTOM_MINUTES: 4,
    TOP_MINUTES: 11,
    BOTTOM_HOURS: 4,
    TOP_HOURS: 4
};

module.exports = LAMP_TOTAL;
