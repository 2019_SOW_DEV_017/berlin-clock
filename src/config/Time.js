var TIME = {
    ALLOWED_PATTERN: "([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])",
    INVALID_MESSAGE: "Invalid time format",
    SEPARATOR: ":",
    HOURS_POSITION: 0,
    MINUTES_POSITION: 1,
    SECONDS_POSITION: 2
};

module.exports = TIME;
