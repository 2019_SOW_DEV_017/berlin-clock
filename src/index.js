var BerlinClock = require("../src/BerlinClock");
var berlinClock = new BerlinClock();

document.getElementById("btnConvert").addEventListener("click", function () {

    document.getElementById("berlinTime").innerHTML = berlinClock.clock(document.getElementById("timeToConvert").value);
});
