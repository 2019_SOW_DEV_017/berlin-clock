var LAMP = require('./config/Lamp');
var LAMP_TOTAL = require('./config/LampTotal');
var TIME = require('./config/Time');

var BerlinClock = function () {

    var TOP_MINUTES_PER_LAMP_MINUTES = 5,
        TOP_HOURS_PER_LAMP_HOURS = 5,
        TOP_MINUTES_RED_LAMP_POSITION = 3;
    var timeParts;

    this.bottomMinutes = function (minutes) {
        var numberOfOn, numberOfOff;

        numberOfOn = minutesLeftFromTopMinutesRow(minutes);

        numberOfOff = numberOfOffLamps(LAMP_TOTAL.BOTTOM_MINUTES, numberOfOn);

        return yellowLamps(numberOfOn) + offLamps(numberOfOff);
    };

    this.topMinutes = function (minutes) {
        var numberOfOn, numberOfOff;

        numberOfOn = multipleOf(minutes, TOP_MINUTES_PER_LAMP_MINUTES);

        numberOfOff = numberOfOffLamps(LAMP_TOTAL.TOP_MINUTES, numberOfOn);

        return topMinutesOnLamps(numberOfOn) + offLamps(numberOfOff);
    };

    this.bottomHours = function (hours) {
        var numberOfOn, numberOfOff;

        numberOfOn = hoursLeftFromTopHoursRow(hours);

        numberOfOff = numberOfOffLamps(LAMP_TOTAL.BOTTOM_HOURS, numberOfOn);

        return redLamps(numberOfOn) + offLamps(numberOfOff);
    };

    this.topHours = function (hours) {
        var numberOfOn, numberOfOff;

        numberOfOn = multipleOf(hours, TOP_HOURS_PER_LAMP_HOURS);

        numberOfOff = numberOfOffLamps(LAMP_TOTAL.TOP_HOURS, numberOfOn);

        return redLamps(numberOfOn) + offLamps(numberOfOff);
    };

    this.secondsLamp = function (seconds) {
        return isEven(seconds) ? LAMP.YELLOW : LAMP.OFF;
    };

    this.clock = function (strTime) {
        var output;

        if (!isValidTime(strTime)) {
            output = TIME.INVALID_MESSAGE;
        } else {
            parseTime(strTime);

            var hours, minutes;
            hours = getHours();
            minutes = getMinutes();

            output = this.secondsLamp(getSeconds());
            output += this.topHours(hours);
            output += this.bottomHours(hours);
            output += this.topMinutes(minutes);
            output += this.bottomMinutes(minutes);
        }

        return output;
    };

    function parseTime(strTime) {
        timeParts = strTime.split(TIME.SEPARATOR);
    }

    function getHours() {
        return timePart(TIME.HOURS_POSITION);
    }

    function getMinutes() {
        return timePart(TIME.MINUTES_POSITION);
    }

    function getSeconds() {
        return timePart(TIME.SECONDS_POSITION);
    }

    function timePart(position) {
        return parseInt(timeParts[position]);
    }

    function isValidTime(strTime) {
        var regex = new RegExp(TIME.ALLOWED_PATTERN);

        return regex.test(strTime);
    }

    function isEven(number) {
        return (number % 2) === 0;
    }

    function numberOfOffLamps(totalLamps, numberOfOn) {
        return totalLamps - numberOfOn;
    }

    function topMinutesOnLamps(numberOfOn) {

        var output = "";

        for (var lamp = 1; lamp <= numberOfOn; lamp++) {

            if (isRedLampPosition(lamp)) {
                output += LAMP.RED;
            } else {
                output += LAMP.YELLOW;
            }
        }

        return output;
    }

    function isRedLampPosition(lampIndex) {

        return (lampIndex % TOP_MINUTES_RED_LAMP_POSITION) === 0;
    }

    function redLamps(numberLamps) {
        return lamps(numberLamps, LAMP.RED);
    }

    function yellowLamps(numberLamps) {
        return lamps(numberLamps, LAMP.YELLOW);
    }

    function offLamps(numberLamps) {
        return lamps(numberLamps, LAMP.OFF);
    }

    function lamps(numberLamps, lampSign) {

        var output = "";

        for (var lamp = 0; lamp < numberLamps; lamp++) {
            output += lampSign;
        }

        return output;
    }

    function minutesLeftFromTopMinutesRow(minutes) {
        return remainder(minutes, TOP_MINUTES_PER_LAMP_MINUTES);
    }

    function hoursLeftFromTopHoursRow(hours) {
        return remainder(hours, TOP_HOURS_PER_LAMP_HOURS);
    }

    function remainder(number, divisor) {
        return number % divisor;
    }

    function multipleOf(total, multiplier) {
        return Math.floor(total / multiplier);
    }
};

module.exports = BerlinClock;
